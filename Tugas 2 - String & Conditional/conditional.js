// if-else

var nama = "Junaedi"
var peran = "Werewolf"

if (nama.length == 0) {
    console.log('Nama harus diisi!')
} else if (peran.length == 0) {
    console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game!')
} else {
    console.log('Selamat datang di Dunia Werewolf, ' + nama)
    if (peran == 'Penyihir') {
        console.log('Halo Penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf!')
    } else if (peran == 'Guard'){
        console.log('Halo Guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf.')
    } else if (peran == 'Werewolf'){
        console.log('Halo Werewolf ' + nama + ', Kamu akan memakan mangsa setiap malam!')
    } else {
        console.log('Halo  ' + nama + ', pilih peran Penyihir, Guard, atau Werewolf')
    }
}

// Switch Case

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var bulanEjaan = '';

switch (bulan){
    case 1 : { bulanEjaan = 'Januari'; break; }
    case 2 : { bulanEjaan = 'Februari'; break; }
    case 3 : { bulanEjaan = 'Maret'; break; }
    case 4 : { bulanEjaan = 'April'; break; }
    case 5 : { bulanEjaan = 'Mei'; break; }
    case 6 : { bulanEjaan = 'Juni'; break; }
    case 7 : { bulanEjaan = 'Juli'; break; }
    case 8 : { bulanEjaan = 'Agustus'; break; }
    case 9 : { bulanEjaan = 'September'; break; }
    case 10 : { bulanEjaan = 'Oktober'; break; }
    case 11 : { bulanEjaan = 'November'; break; }
    case 12 : { bulanEjaan = 'Desember'; break; }
    default : { bulanEjaan = '' ; }
}

if (hari < 1 || hari > 31){
    console.log('Input hari harus antara 1 - 31')
} else if (bulan < 1 || bulan > 12 ){
    console.log('Input bulan harus antara 1 - 12')
} else if (tahun < 1900 || tahun > 2200){
    console.log('Input tahun harus antara 1900 - 2200')
} else {
    console.log(hari + ' ' + bulanEjaan + ' ' + tahun)
}