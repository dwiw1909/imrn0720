import React from 'react';
import {View} from 'react-native';
import Tugas12 from './Tugas/Tugas12/App';
import Tugas13 from './Tugas/Tugas13/App';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15Soal1 from './Tugas/Tugas15/index';
import Tugas15Soal2 from './Tugas/Tugas15/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz3/index';

const App = () => {
  return (
    // <View style={{flex: 1}}>
      // <Tugas12 />
      // <Tugas13 />
      // <Tugas14 />
      // <Tugas15Soal1 />
      // <Tugas15Soal2 />
      <Quiz3 />
    //</View>
  );
};

export default App;
