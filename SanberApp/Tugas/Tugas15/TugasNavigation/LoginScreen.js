import React from 'react';
import {StyleSheet, Text, View, Image, TextInput, ScrollView, TouchableOpacity} from 'react-native';

const LoginScreen = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <Image source={require('./images/logo.png')} style={styles.logo} />
      <View style={styles.title}>
        <Text style={styles.titleText}>Login</Text>
      </View>
      <View style={styles.inputTextGrup}>
        <Text style={styles.inputTitle}>Username/Email</Text>
        <TextInput style={styles.inputText} />
      </View>
      <View style={styles.inputTextGrup}>
        <Text style={styles.inputTitle}>Password</Text>
        <TextInput secureTextEntry={true} style={styles.inputText} />
      </View>
      <View style={styles.inputButton}>
        <TouchableOpacity style={styles.masukButton}>
          <Text style={styles.textButton} onPress={() => navigation.navigate('Tabs', {screen: 'SkillScreen'})}>Masuk</Text>
        </TouchableOpacity>
        <Text style={styles.atauText}>atau</Text>
        <TouchableOpacity style={styles.daftarButton}>
          <Text style={styles.textButton}>Daftar ?</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {},
  logo: {
    marginTop: 63,
    width: '100%',
  },
  titleText: {
    paddingTop: 56,
    fontSize: 24,
    color: '#003366',
    marginBottom: 40,
  },
  title: {
    alignItems: 'center',
  },
  inputTextGrup: {
    marginLeft: 40,
    marginBottom: 16,
  },
  inputText: {
    height: 48,
    width: 294,
    borderWidth: 1,
    borderColor: '#003366',
    backgroundColor: '#FFFFFF',
  },
  inputTitle: {
    fontSize: 16,
    color: '#003366',
    marginBottom: 4,
  },
  daftarButton: {
    backgroundColor: '#003366',
    height: 40,
    width: 140,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 179,
  },
  textButton: {
    fontSize: 24,
    color: '#FFFFFF',
  },
  inputButton: {
    alignItems: 'center',
  },
  atauText: {
    marginVertical: 16,
    fontSize: 24,
    color: '#3EC6FF',
  },
  masukButton: {
    backgroundColor: '#3EC6FF',
    height: 40,
    width: 140,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
  },
});
