import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const AboutScreen = () => {
  return (
    <ScrollView>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.title}>Tentang Saya</Text>
        <Icon name="person-circle" size={200} />
        <Text style={styles.name}>Mukhlis Hanafi</Text>
        <Text style={styles.job}>React Native Developer</Text>
        {/* Portofolio */}
        <View style={styles.portoContainer}>
          <Text style={styles.porto}>Portofolio</Text>
          <View style={styles.line} />
          <View style={styles.portoItem}>
            <View style={{alignItems: 'center'}}>
              <Icon name="logo-bitbucket" size={40} color="#3EC6FF" />
              <Text style={{marginTop: 10}}>@mukhlis</Text>
            </View>
            <View style={{alignItems: 'center'}}>
              <Icon name="logo-github" size={40} color="#3EC6FF" />
              <Text style={{marginTop: 10}}>@mukhlis</Text>
            </View>
          </View>
        </View>
        {/* Hubungi Saya */}
        <View style={styles.callAbout}>
          <Text style={styles.callaboutText}>Hubungi Saya</Text>
          <View style={styles.line} />
          <View style={{marginLeft: 89}}>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 19}}>
              <Icon name="logo-facebook" size={39} color='#3EC6FF' />
              <Text style={{fontSize: 16, fontWeight: 'bold', marginLeft: 19}}>mukhlis.hanafi</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25}}>
              <Icon name="logo-instagram" size={39} color='#3EC6FF' />
              <Text style={{fontSize: 16, fontWeight: 'bold', marginLeft: 19}}>@mukhlis_hanafi</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 25}}>
              <Icon name="logo-twitter" size={39} color='#3EC6FF' />
              <Text style={{fontSize: 16, fontWeight: 'bold', marginLeft: 19}}>@mukhlis</Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  title: {
    marginTop: 64,
    fontSize: 36,
    color: '#003366',
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 24,
    color: '#003366',
  },
  job: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 8,
    color: '#3EC6FF',
  },
  portoContainer: {
    width: 359,
    height: 140,
    borderRadius: 16,
    backgroundColor: '#EFEFEF',
    marginTop: 16,
    marginHorizontal: 8,
  },
  porto: {
    fontSize: 18,
    marginTop: 5,
    marginLeft: 8,
    color: '#003366',
  },
  line: {
    borderWidth: 1,
    borderColor: '#003366',
    marginHorizontal: 8,
    marginTop: 8,
  },
  portoItem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  callAbout: {
    width: 359,
    height: 251,
    borderRadius: 16,
    backgroundColor: '#EFEFEF',
    marginTop: 9,
    marginHorizontal: 8,
    marginBottom: 64,
  },
  callaboutText: {
    fontSize: 18,
    color: '#003366',
    marginTop: 7,
    marginLeft: 8
  }
});
