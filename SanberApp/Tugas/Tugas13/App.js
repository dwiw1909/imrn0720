import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Register from './RegisterScreen';
import Login from './LoginScreen';
import About from './AboutScreen';

const app = () => {
  return (
    <View>
      <Register />
      {/* <Login /> */}
      {/* <About /> */}
    </View>
  );
};

export default app;

const styles = StyleSheet.create({});
