import React from 'react';
import {StyleSheet, Text, View, Image, FlatList} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {items} from '../skillData.json';
import Skill from './Skill';

const SkillScreen = () => {
  return (
    <View style={{flex: 1}}>
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 1}} />
        <Image
          source={require('../images/logo.png')}
          style={{flex: 1, justifyContent: 'center',  height: 60}}
        />
      </View>
      <View style={{flexDirection: 'row', alignItems:'center', paddingTop: 3, paddingLeft: 19}}>
        <Icon name="account-circle" size={26} color="#3EC6FF" style={{paddingRight: 11}} />
        <View>
          <Text style={{fontSize: 12, color:"#000000"}}>Hai,</Text>
          <Text style={{fontSize: 16, color:"#003366"}}>Mukhlis Hanafi</Text>
        </View>
      </View>
      <Text style={{fontSize: 36, color:"#003366", paddingTop: 16, paddingLeft: 16}}>SKILL</Text>
      <View style={{height: 4, backgroundColor:"#3EC6FF", marginHorizontal: 16}} />
      <View style={{paddingHorizontal: 16, paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-around'}}>
        <View style={{height: 32, backgroundColor:"#B4E9FF", borderRadius: 8}}>
          <Text style={{fontSize: 12, fontWeight: 'bold', color:"#003366", paddingHorizontal: 8, paddingVertical: 9}}>Library / Framework</Text>
        </View>
        <View style={{height: 32, backgroundColor:"#B4E9FF", borderRadius: 8}}>
          <Text style={{fontSize: 12, fontWeight: 'bold', color:"#003366", paddingHorizontal: 8, paddingVertical: 9}}>Library / Framework</Text>
        </View>
        <View style={{height: 32, backgroundColor:"#B4E9FF", borderRadius: 8}}>
          <Text style={{fontSize: 12, fontWeight: 'bold', color:"#003366", paddingHorizontal: 8, paddingVertical: 9}}>Library / Framework</Text>
        </View>
      </View>
      {/* Skill */}
      <View style={{flex: 1}}>
        <FlatList
          data={items}
          renderItem={({item}) => (<Skill data={item} />)}
          keyExtractor={item => item.id.toString()}>
        </FlatList>
      </View>
    </View>
  );
};

export default SkillScreen;
