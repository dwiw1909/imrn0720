import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';

import Note from './Note';

const Main = () => {
  const [notes, setNotes] = useState([]);

  const [text, setText] = useState('');
  const changehandler = (val) => setText(val);

  const addNote = () => {
    if (!text) {
      alert('masukan note dulu');
    } else {
      let d = new Date();
      setNotes((prevNotes) => {
        return [
          ...prevNotes,
          {
            date: `${d.getFullYear()}/${d.getMonth() + 1}/${d.getDate()}`,
            note: text,
          },
        ];
      });
      setText('');
    }
  };

  const delNote = (id) => {
    setNotes((prevNotes) => {
      prevNotes.splice(id, 1);
      return [...prevNotes];
    });
  };

  const render = notes.map((val, key) => {
    return <Note key={key} val={val} del={() => delNote(key)} />;
  });

  return (
    <View style={styles.container}>
      {/* Header */}
      <View style={styles.header}>
        <Text style={styles.headerText}>- Noter -</Text>
      </View>
      {/* Body */}
      <ScrollView>{render}</ScrollView>
      {/* Footer */}
      <View style={styles.footer}>
        <TextInput
          style={styles.textInput}
          placeholder=">note"
          placeholderTextColor="white"
          underlineColorAndroid="transparent"
          onChangeText={changehandler}
          value={text}
        />
      </View>
      <TouchableOpacity onPress={addNote} style={styles.addButton}>
        <Text style={styles.addButtonText}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Main;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#E91E63',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#ddd',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#fff',
    padding: 20,
    backgroundColor: '#252525',
    borderTopWidth: 2,
    borderTopColor: '#ededed',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 90,
    backgroundColor: '#E91E63',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
});
