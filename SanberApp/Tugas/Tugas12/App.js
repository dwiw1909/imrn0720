import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './components/videoItem';
import Datas from './data.json';

const app = () => {
  return (
    <View style={{flex: 1}}>
      {/* header */}
      <View style={styles.header}>
        <Image source={require('./images/logo.png')} style={styles.logo} />
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <Icon name="search" size={25} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" size={25} style={{marginLeft: 25}} />
          </TouchableOpacity>
        </View>
      </View>
      {/* Body */}
      <View style={styles.body}>
        <FlatList
          data={Datas.items}
          renderItem={(video) => <VideoItem data={video.item} />}
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={styles.sparator} />}
        />
      </View>
      {/* Footer */}
      <View style={styles.footer}>
        <TouchableOpacity style={styles.itemFooter}>
          <Icon name="home" size={25} />
          <Text style={styles.iconTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemFooter}>
          <Icon name="whatshot" size={25} />
          <Text style={styles.iconTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemFooter}>
          <Icon name="subscriptions" size={25} />
          <Text style={styles.iconTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.itemFooter}>
          <Icon name="folder" size={25} />
          <Text style={styles.iconTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default app;

const styles = StyleSheet.create({
  header: {
    height: 50,
    backgroundColor: 'white',
    elevation: 3,
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    height: 22,
    width: 98,
  },
  body: {
    flex: 1,
  },
  footer: {
    height: 60,
    backgroundColor: 'white',
    borderTopWidth: 0.5,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 30,
  },
  itemFooter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4,
  },
  videoItems: {
    height: 300,
    width: '100%',
    backgroundColor: 'pink',
  },
  sparator: {
    height: 0.5,
    backgroundColor: '#E5E5E5',
  },
});
