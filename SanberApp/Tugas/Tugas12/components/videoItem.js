import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const videoItem = (props) => {
  let data = props.data;
  return (
    <View style={styles.container}>
      <Image
        source={{uri: data.snippet.thumbnails.medium.url}}
        style={styles.thumb}
      />
      <View style={styles.titleDesc}>
        <Image
          source={{uri: data.snippet.thumbnails.medium.url}}
          style={styles.channelIcon}
        />
        <View style={{flex: 1}}>
          <TouchableOpacity>
            <Text numberOfLines={2} style={styles.title}>{data.snippet.title}</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.channel}>
              {`${data.snippet.channelTitle} · ${nFormatter(data.statistics.viewCount, 1)} views · 3 months ago`}
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity>
          <Icon name="more-vert" size={20} color="#999999" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default videoItem;

const nFormatter = (num, digits) => {
  var si = [
    { value: 1, symbol: "" },
    { value: 1E3, symbol: "k" },
    { value: 1E6, symbol: "M" },
    { value: 1E9, symbol: "G" },
    { value: 1E12, symbol: "T" },
    { value: 1E15, symbol: "P" },
    { value: 1E18, symbol: "E" }
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  thumb: {
    height: 200,
  },
  channelIcon: {
    height: 50,
    width: 50,
    borderRadius: 25,
    marginRight: 15,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#3C3C3C',
  },
  channel: {
    fontSize: 14,
  },
  titleDesc: {
    flexDirection: 'row',
    paddingTop: 10,
  },
});
