import React, {useEffect} from 'react';
import {StyleSheet, View, Image} from 'react-native';

const Splash = ({navigation}) => {

  useEffect(() => {
    setTimeout(() => {
      navigation.push('Register');
    }, 2000);
  },[]);
  return (
    <View style={styles.container}>
      <View style={styles.bg} />
      <Image source={require('./logo.png')} style={styles.logo} />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 25,
  },
  bg: {
    height: 300,
    width: 300,
    backgroundColor: '#211F65',
    opacity: 0.1,
    borderRadius: 150,
  },
  logo: {
    position: 'absolute',
  },
});
