// SOAL 1
class Score {
    constructor(subject, points, email){
        this.email = email
        this.subject = subject
        this.points = points
    }
    averange(){
        if(this.points.length > 0){
            let avg = 0
            this.points.forEach(point => avg += point);
            return avg/this.points.length
        } else {
            return this.points
        }
    }
    predikat(score){
        if(score > 90){
            return 'honour'
        } else if (score > 80){
            return 'graduate'
        } else if (score > 70){
            return 'participant'
        }
        return 'fail'
    }
}
//TEST CASE
// var andi = new Score('andi', [10, 9], 'andi@sanber.code')
// console.log(andi.averange());


// SOAL 2
const data =
    [
        ["email", "quiz-1", "quiz-2", "quiz-3"],
        ["hilmy@mail.com", 78, 89, 90],
        ["mukhlis@mail.com", 95, 85, 88]
    ]

const viewScores = (datas, query) => {
    let output = []
    let idx
    for(let i in datas){
        if(i == 0){
            for(let j in datas[i]){
                if(query == datas[i][j]){
                    idx = j
                }
            }
        } else {
            let obj = new Score (query, datas[i][idx], datas[i][0])
            output.push(obj)
        }
    }
    return console.log(output)
}
//TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


// SOAL 3
const recapScores = (datas) => {
    for(let i in datas){
        if(i > 0){
            let siswa = new Score('rekap', datas[i].slice(1), datas[i][0])
            let avg = siswa.averange()
            let predikat = siswa.predikat(avg)
            console.log(`${i}. Email: ${siswa.email} \n Rata-rata: ${avg.toFixed(1)} \n Predikat: ${predikat}`);
        }
    }
}

recapScores(data)