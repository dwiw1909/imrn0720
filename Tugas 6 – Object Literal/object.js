// Soal 1
function arrayToObject(data){
    if (data.length === 0){
        return console.log('')
    } else {
        let now = new Date
        let thisYear = now.getFullYear()
        for (let i = 0; i < data.length; i++){
            let obj = {}
            obj.firstName = data[i][0]
            obj.lastName = data[i][1]
            obj.gender = data[i][2]
            if (data[i][3] > thisYear || data[i][3] === undefined){
                obj.age = 'Invalid birth year'
            } else {
                obj.age = thisYear - data[i][3]
            }
            console.log(i + 1 + '. ' + obj.firstName + ' ' + obj.lastName + ' : ');
            console.log(obj);
        }
    }
}

var input = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]]
arrayToObject(input)

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

arrayToObject([]) // ""



// Soal 2
function shoppingTime(memberId, money) {
    let barang = [['Sepatu Stacattu', 1500000], ['Baju Zoro', 500000], ['Baju H&N', 250000], ['Sweater Uniklooh', 175000], ['Casing Handphone', 50000]]
    if(memberId == '' || memberId == undefined){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        let obj = {}
        obj.memberId = memberId
        obj.money = money
        let list = []
        let kondisi = true
        for(let i = 0; i < barang.length; i++){
            if(money < 50000){
                i = barang.length
            } else if (money >= barang[i][1]){
                list.push(barang[i][0])
                money -= barang[i][1]
            }
        }
        obj.listPurchased = list
        obj.changeMoney = money
        // console.log(list);
        return obj
    }
}
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



  // Soal 3
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let hasil = []
    if(arrPenumpang.length != 0){
        for(let i = 0; i < arrPenumpang.length; i++){
            let obj = {}
            obj.penumpang = arrPenumpang[i][0]
            obj.naikDari = arrPenumpang[i][1]
            obj.tujuan = arrPenumpang[i][2]
            obj.bayar = 0
            let kondisi = false
            for(let j = 0; j < rute.length; j++){
                if(obj.naikDari == rute[j]){
                    kondisi = true
                } else if(kondisi == true && obj.tujuan == rute[j]){
                    obj.bayar += 2000
                    j = rute.length
                } else if(kondisi == true){
                    obj.bayar += 2000
                }
            }
            hasil.push(obj)
        }
    }
    return hasil
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]