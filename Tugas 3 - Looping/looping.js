// No. 1 Looping While
var arah = 'maju'
var num = 2
while (num >= 2){
    if (arah === 'maju'){
        if (num === 2){
            console.log('LOOPING PERTAMA')
            console.log(num + ' - I love coding')
        } else if (num === 20 ){
            console.log(num + ' - I love coding')
            arah = 'mundur'
            num -= 2
        } else {
            console.log(num + ' - I love coding')
        }
        num += 2
    } else {
        if (num === 20){
            console.log('LOOPING KEDUA')
            console.log(num + ' - I will become a mobile developer')
        } else {
            console.log(num + ' - I will become a mobile developer')
        }
        num -= 2
    }
}

// No. 2 Looping for
for (var angka = 1; angka <= 20; angka++){
    if (angka % 2 != 0  && angka % 3 == 0){
        console.log(angka + ' - I Love Coding')
    } else if (angka % 2 != 0) {
        console.log(angka + ' - Santai')
    } else {
        console.log(angka + ' - Berkualitas')
    }
}

// No. 3 Membuat Persegi Panjang #
for (var a = 1; a <=4; a++){
    console.log('########')
}

// No. 4 Membuat Tangga
var cetak = ''
for (var b = 1; b <= 7; b++){
    for (var c = b; c > 0; c--){
        cetak = cetak + '#'
    }
    console.log(cetak)
    cetak = ''
}

// No. 5 Membuat Papan Catur
for (var d = 1; d <= 8; d++){
    var print = ''
    for (var e = 1; e <= 8; e++){
        if (d % 2 == 0){
            if (e % 2 == 0){
                print += ' '
            } else {
                print += '#'
            }
        } else {
            if (e % 2 == 0){
                print += '#'
            } else {
                print += ' '
            }
        }
    }
    console.log(print)
}