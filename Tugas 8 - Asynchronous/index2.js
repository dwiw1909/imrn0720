var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let waktu = 10000
let idx = 0
function baca(waktu, books){
    if(idx < books.length){
        readBooksPromise(waktu, books[idx])
            .then(sisa => {
                idx++
                baca(sisa, books)
            })
            .catch(err => console.log(err))
    }
}

baca(waktu, books)