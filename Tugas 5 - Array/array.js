// No. 1
function range(num1, num2){
    if (num1 == undefined || num2 == undefined ){
        return -1
    } else {
        var temp = []
        if (num1 > num2){            
            for ( ; num1 >= num2; num1-- ){
                temp.push(num1)
            }
        } else {
            for ( ; num1 <= num2; num1++ ){
                temp.push(num1)
            }
        }
        return temp
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 



// No. 2
function rangeWithStep(startNum, finishNum = 1, step = 1){
    var temp = []
    if (startNum > finishNum){            
        for ( ; startNum >= finishNum; startNum -= step ){
            temp.push(startNum)
        }
    } else {
        for ( ; startNum <= finishNum; startNum += step ){
            temp.push(startNum)
        }
    }
    return temp
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



// No. 3
function sum(a, b, c){
    var arr = rangeWithStep(a, b, c)
    var ttl = 0
    for(var d = 0; d < arr.length; d++){
        ttl += arr[d]
    }
    return ttl
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 



// No. 4
function dataHandling(data){
    for(var a = 0; a < data.length; a++){        
        console.log('Nomor ID: ' + data[a][0]);
        console.log('Nama Lengkap: ' + data[a][1]);
        console.log('TTL: ' + data[a][2] + ' ' +  data[a][3]);
        console.log('Hobi: ' + data[a][4]);
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)



// No. 5
function balikKata(kata){
    var hasil = ''
    for(var a = kata.length-1; a >= 0; a--){
        hasil += kata[a]
    }
    return hasil
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 



// No. 6
var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
function dataHandling2(arr){
    var hasil = []
    arr.splice(1, 2, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung')
    arr.splice(4, 1, 'Pria', 'SMA Internasional Metro')
    console.log(arr);
    var ttl = arr[3].split('/')
    var bln = ''
    switch(ttl[1]){
        case '01': bln = 'Januari'; break;
        case '02': bln = 'Februari'; break;
        case '03': bln = 'Maret'; break;
        case '04': bln = 'April'; break;
        case '05': bln = 'Mei'; break;
        case '06': bln = 'Juni'; break;
        case '07': bln = 'Juli'; break;
        case '08': bln = 'Agustus'; break;
        case '09': bln = 'September'; break;
        case '10': bln = 'Oktober'; break;
        case '11': bln = 'November'; break;
        case '12': bln = 'Desember';
    }
    console.log(bln);
    var ttlNum = []
    for (var a = 0; a < ttl.length; a++){
        ttlNum.push(Number(ttl[a]))
    }
    var ttlNumSort = ttlNum.sort(function(a, b){ return b - a });
    var ttlStrSort = []
    for(var a = 0; a < ttlNumSort.length; a++){
        if((a === 1 || a === 2) && ttlNumSort[a] < 10){
            ttlStrSort.push('0' + String(ttlNumSort[a]))
        } else {
            ttlStrSort.push(String(ttlNumSort[a]))
        }
    }
    console.log(ttlStrSort);
    console.log(ttl.join('-'));
    console.log(arr[1].slice(0, 15));
}
dataHandling2(data)