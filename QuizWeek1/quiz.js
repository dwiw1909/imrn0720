// Soal 1
function balikString(kata){
    let hasil = ''
    for(let i = kata.length-1; i >= 0; i--){
        hasil += kata[i]
    }
    return hasil
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah



// Soal 2
function palindrome(kata){
    if(balikString(kata) === kata){
        return true
    } else {
        return false
    }

}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false



//Soal 3
function bandingkan(num1 = 0, num2 = 0){
    if (num1 < 0 || num2 < 0|| num1 === num2){
        return -1
    } else if(num1 > num2){
        return num1
    } else {
        return num2
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18



// Soal 4
function DescendingTen(num){
    if(num === undefined || num < 10){
        return -1
    } else {
        let hasil = ''
        for(let i = num; i > num-10; i--){
            hasil += String(i) + ' '
        }
        return hasil
    }
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1



// Soal 5
function ularTangga(size = 10){
    let kekanan = true
    for(let i = size; i > 0; i--){
        let temp = ''
        let jml = i * size
        if(kekanan){
            for(let j = size; j > 0; j--){
                temp += String(jml) + ' '
                jml--
            }
            kekanan = false
        } else {
            jml -= 9
            for(let j = size; j > 0; j--){
                temp += String(jml) + ' '
                jml++
            }
            kekanan = true
        }
        console.log(temp);
    }
}

ularTangga()
